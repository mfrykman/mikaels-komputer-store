(function (window, document, undefined) {
  window.onload = init;

  function init() {
    //Bank section//
    const balanceElement = document.getElementById("balance");
    const loanElement = document.getElementById("loan");

    let loanButtonElement = document.getElementById("loanButton");
    let repayButtonElement = document.getElementById("repayButton");
    let buyButtonElement = document.getElementById("buyButton");

    let balance = 200;

    //Work section//
    let walletElement = document.getElementById("wallet");
    let putinbankElement = document.getElementById("putinbank");
    const laborElement = document.getElementById("labor");
    const laptopsElement = document.getElementById("laptops");
    const selectedLaptopElement = document.getElementById("selected");

    //Wallet
    let wallet = 0;

    var selectedItem = {};
    let laptops = [];
    let loanAmount = 0;

    console.log("Your wallet is:" + wallet);
    console.log("Your balance is:" + balance);

    console.log(balanceElement.innerHTML);

    function updateDOM() {
      balanceElement.innerHTML = balance;
      walletElement.innerHTML = wallet;
      loanElement.innerHTML = loanAmount;

      selectedLaptopElement.innerHTML = JSON.stringify(selectedItem);
    }

    updateDOM();

    fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
      .then((response) => response.json())
      .then((data) => (laptops = data))
      .then((laptops) => addLaptopsToDisplay(laptops));

    const addLaptopsToDisplay = (laptops) => {
      console.log(laptops[0]);
      selectedItem = laptops[0];
      updateDOM();
      laptops.forEach((x) => addLaptopToDisplay(x));
    };

    const addLaptopToDisplay = (laptop) => {
      const laptopElement = document.createElement("option");
      laptopElement.value = laptop.id;
      laptopElement.appendChild(document.createTextNode(laptop.title));
      laptopsElement.appendChild(laptopElement);
    };

    function labor() {
      wallet += 100;
    }

    function loan() {
      const amount = prompt(
        "Please enter the amount of money you wish to loan"
      );

      if (amount > 2 * balance || loanAmount > 0) {
        alert(
          "Loan declined: You can only loan twice the amount currently in your wallet at most and only have one loan at a time"
        );
      } else {
        balance += Number(amount);
        loanAmount = Number(amount);
        alert("Your loan has been added?");
      }
    }

    function buy(amount) {
      if (amount <= balance) {
        balance -= amount;
        alert("Your have bought the laptop");
      } else {
        alert("You dont have enough money to buy the laptop");
      }
    }

    function putinbank() {
      balance += wallet;
      wallet = 0;
    }

    function repay() {
      if (loanAmount < balance) {
        balance -= loanAmount;
        loanAmount = 0;

        alert("Your loan has been repayed");
      } else {
        alert("You dont have enough money to repay your loan, work some more");
      }
    }

    // Buttons

    laborElement.addEventListener("click", function () {
      wallet += 100;
      updateDOM();
    });
    putinbankElement.addEventListener("click", function () {
      putinbank();
      updateDOM();
    });
    loanButtonElement.addEventListener("click", function () {
      loan();
      updateDOM();
    });
    repayButtonElement.addEventListener("click", function () {
      repay();
      updateDOM();
    });

    buyButtonElement.addEventListener("click", function () {
      buy(selectedItem.price);
      updateDOM();
    });

    const handleChange = (e) => {
      console.log(e.target.selectedIndex);
      selectedItem = laptops[e.target.selectedIndex];
      updateDOM();
    };

    laptopsElement.addEventListener("change", handleChange);
  }
})(window, document, undefined);
